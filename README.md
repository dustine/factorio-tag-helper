# Factorio Tag Helper

GUI utility to ease formatting rich text and inserting tags (those emoji like icons) on Factorio 0.17+

## New in 0.2

Formatting rich text is now possible over at a new tab on the far right, `Format`. Try it out!

## Description

This mod is meant to make dealing with [Rich Text encoding](https://wiki.factorio.com/Rich_text) on Factorio a tad easier (and without any outside reference, to boot). Press ALT + PERIOD, by default, to activate it.

![Basic output over the achievements tab](https://i.imgur.com/WDd92Eq.png)

A menu will appear with all possible tags, just click on any at your discretion and they'll plomp down on the output below. Toggle the output to expand the rich text tags if one wants to tweak them manually.

![Expanded output with search query for lead on items](https://i.imgur.com/LgTXJOn.png)

Oh, and you can search by typing on the upper bar. Easy peasy.

## Thank yous

To my co-players over the Fungeon, for giving me the energy for this project and deal with this personal pet peeve.
And to the developer of [Factory Planner](https://mods.factorio.com/mod/factoryplanner), who their code was both an inspiration, and indispensable help with stuff like search and sorting.

## Warning

This is a work-in-progress mod and its full spec is rather hampered by the current limitations of the Factorio Modding API, but specially my lack of time. I did this for fun over an extended weekend (make it two now), so um, future development might be slow to non-existent? Just [branch it over here](https://gitlab.com/dustine/factorio-tag-helper) if you wish to update it in my lieu, and I apologize in advance for my spaghetti code.

## To-Do

- [ ] Better README.md
- [ ] Clean this filthy, filthy codebase
- [ ] Add HSL and Hex support for custom colors
- [ ] Spruce up the interface

## Would-Love-To

- [ ] Color & tag historic
- [ ] Fuzzy search
- [ ] Translation support (asynchronous and player-based, so will require a rewrite)
- [ ] Have formatting respect the output's selection (requires API request)
