---------------------------------------------------------------------------------------------------
Version: 0.3.1
Date: 17.10.2019
  Bugfixes:
    - Fixed crash when fluid groups shared name with an item group (as seen on Yuoki Industries)
    - Fixed crash when swapping to an ungrouped tab while having a search query active
    - Searchbox now (properly) clears out when tag helper interface is closed
  Tweaks:
    - Focusing on the search bar (CTRL/CMD-F by default) now follows basegame behaviour
---------------------------------------------------------------------------------------------------
Version: 0.3.0
Date: 09.10.2019
  Features:
    - Holding Control when adding a tag or text colour will add it to delimitators on the output, if relevant (check tooltips for more info)
---------------------------------------------------------------------------------------------------
Version: 0.2.2
Date: 06.10.2019
  Bugfixes:
    - Fixed crash when creating full fluids or signal tags
---------------------------------------------------------------------------------------------------
Version: 0.2.1
Date: 05.10.2019
  Tweaks:
    - Updated info.json, formatting isn't coming soon anymore, it's here
    - Synced README.md with the mod portal
---------------------------------------------------------------------------------------------------
Version: 0.2.0
Date: 05.10.2019
  Features:
    - Added formatting tab, the other half of rich text formatting
      - Live preview
      - Color and font presets
      - Custom color support via RGB sliders
  Bugfixes:
    - Searching on a tab won't mess the spacing of other tabs
    - Tag groups start selected when the interface is initialized, or when search removes the selected group
  Tweaks:
    - Search is now only done within the current tab, should improve performance while searching
    - All in-game strings are localized now (only in English, for now)
    - Redid how some components were sized, interface should look better and be more dynamic now
    - Repo-only files aren't included on the mod .zip anymore (every kB counts!)
---------------------------------------------------------------------------------------------------
Version: 0.1.2
Date: 30.09.2019
  Features:
    - Added item-group category (completely forgot about it >o<)
  Bugfixes:
    - Fixed crash on loading existing savegames (for real this time)
    - Utility (and potentially others with '_' on their sprite path) tags won't get cut-off anymore
    - Utility and equipment tags won't output as invalid full tags anymore (which they would always be)
  Minor:
    - Right-click functionality changed, it now does the opposite of whatever left-click would do over tags
  Tweaks:
    - Search terms now ignore whitespace and '-' characters, increasing the odd for desired matches
---------------------------------------------------------------------------------------------------
Version: 0.1.1
Date: 30.09.2019
  Bugfixes:
    - Added player-side sanity check to discard invalid sprite paths immediately
    - Now using iron *plates* instead of ingots for mostly-universal compatibility with tooltips
---------------------------------------------------------------------------------------------------
Version: 0.1.0
Date: 30.09.2019
  Features:
    - Rich text tag selection menu, tabbed by tag category
      - Items (together with fluids and signals), tiles, entities, recipes, technologies, achievements, equipment icons, and (hand-picked) utility icons
    - Toggle between full tags and [img=...] icon-only
    - Output box where you can toggle between simple and expanded (rich text doesn't render) format for manual tweaking
    - (Extremely) mod agnostic
  Minor:
    - Right-click on a tag button will always give you the icon format