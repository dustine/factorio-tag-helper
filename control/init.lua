-- game.item_prototypes
-- game.entity_prototypes
-- game.technology_prototypes
-- game.recipe_prototypes
-- game.item_group_prototypes
-- game.fluid_prototypes
-- game.tile_prototypes
-- game.virtual_signal_prototypes
-- game.achievements_prototypes
-- game.equipment_prototypes

-- no game.utilities_prototypes ...

local generator = require("generator")

local function run_on_load()
end

local categories = {
  items = "item",
  fluids = "fluid",
  signals = "virtual-signal",
  tiles = "tile",
  entities = "entity",
  recipes = "recipe",
  technologies = "technology",
  achievements = "achievement",
  equipment = "equipment",
  groups = "item-group",
  utilities = "utility"
}

local function global_init()
  generator.start(categories)

  run_on_load()
end

-- Sets up global data structure of the mod
script.on_init(global_init)

-- For migrations and data structure reload
script.on_configuration_changed(
  function()
    -- We are not a smart mod, how do we know when a mod has added or removed a signal?
    -- We don't. Just rebuilt it all...
    generator.start(categories)
    -- ... and clear the player's dialog while at it
    for _, player in pairs(game.players) do
      if player.gui.screen.th_frame_main_dialog then
        player.gui.screen.th_frame_main_dialog.destroy()
      end
    end
  end
)

-- Create lua data caches (if needed)
script.on_load(run_on_load)
