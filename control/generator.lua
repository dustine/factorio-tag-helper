local utility_names = require("utility-sprites")

-- ty Factory Planner 💙
local generator = {}

-- Sorts the objects according to their group, subgroup and order
local function create_object_tree(objects)
  local function sorting_function(a, b)
    if a.group then
      if a.group.order < b.group.order then
        return true
      elseif a.group.order > b.group.order then
        return false
      end
    end
    if a.subgroup then
      if a.subgroup.order < b.subgroup.order then
        return true
      elseif a.subgroup.order > b.subgroup.order then
        return false
      end
    end
    if a.order < b.order then
      return true
    elseif a.order > b.order then
      return false
    end
    return a.name < b.name
  end

  table.sort(objects, sorting_function)
  return objects
end

-- Generates a table imitating the a LuaGroup to avoid lua-cpp bridging
local function generate_group_table(group)
  return {name = group.name, localised_name = group.localised_name, order = group.order}
end

-- (re)creates all tag tabels on global
function generator.start(categories)
  global.categories = categories
  for fun, type in pairs(categories) do
    global["all_" .. fun] = generator["all_" .. fun](type)
  end
end

local function all_grouped(type, blacklist)
  local tags = {type = type, groups = {}}

  for _, proto in pairs(game[type .. "_prototypes"]) do
    if not (blacklist and blacklist(proto)) then
      local tag = {
        name = proto.name,
        localised_name = proto.localised_name,
        order = proto.order,
        group = generate_group_table(proto.group),
        subgroup = generate_group_table(proto.subgroup)
      }

      if not tags.groups[tag.group.name] then
        tags.groups[tag.group.name] = {}
      end

      table.insert(tags.groups[tag.group.name], tag)
    end
  end

  for _, group in pairs(tags.groups) do
    create_object_tree(group)
  end

  return tags
end

local function all_ungrouped(type, has_subgroups)
  local tags = {type = type, tags = {}}

  for _, proto in pairs(game[type .. "_prototypes"]) do
    local tag = {
      name = proto.name,
      localised_name = proto.localised_name,
      order = proto.order,
      subgroup = has_subgroups and generate_group_table(proto.subgroup)
    }

    table.insert(tags.tags, tag)
  end

  create_object_tree(tags.tags)
  return tags
end

function generator.all_items()
  return all_grouped(
    "item",
    function(proto)
      -- items can be flagged as "hidden" so let's take care of that advantage
      -- return proto.has_flag("hidden")
    end
  )
end

function generator.all_fluids()
  return all_grouped("fluid")
end

function generator.all_signals()
  return all_ungrouped("virtual_signal", true)
end

function generator.all_tiles()
  return all_ungrouped("tile")
end

function generator.all_entities()
  return all_grouped(
    "entity",
    function(proto)
      -- some entities types, by hardcoded definition, have no sprite, and there doesn't seem to be a way to verify this from the game prototype
      -- as they're game utilities, basically, it's okay to filter them out
      -- maybe a whitelist would be a better idea...

      local entity_blacklist_types = {
        arrow = true,
        beam = true,
        decorative = true,
        explosion = true,
        fire = true,
        particle = true,
        projectile = true,
        smoke = true,
        sticker = true,
        stream = true
      }
      entity_blacklist_types["artillery-projectile"] = true
      entity_blacklist_types["deconstructible-tile-proxy"] = true
      entity_blacklist_types["entity-ghost"] = true
      entity_blacklist_types["flame-thrower-explosion"] = true
      entity_blacklist_types["flying-text"] = true
      entity_blacklist_types["highlight-box"] = true
      entity_blacklist_types["item-entity"] = true
      entity_blacklist_types["item-request-proxy"] = true
      entity_blacklist_types["leaf-particle"] = true
      -- entity_blacklist["optimized-decorative"] = true
      entity_blacklist_types["particle-source"] = true
      entity_blacklist_types["rocket-silo-rocket"] = true
      entity_blacklist_types["rocket-silo-rocket-shadow"] = true
      entity_blacklist_types["smoke-with-trigger"] = true
      entity_blacklist_types["speech-bubble"] = true
      entity_blacklist_types["tile-ghost"] = true

      return entity_blacklist_types[proto.type]
    end
  )
end

function generator.all_recipes()
  return all_grouped("recipe")
end

function generator.all_technologies()
  return all_ungrouped("technology", false)
end

function generator.all_achievements()
  return all_ungrouped("achievement", false)
end

function generator.all_equipment()
  return all_ungrouped("equipment", false)
end

function generator.all_groups()
  -- TODO: what is order_in_recipe ?
  return all_ungrouped("item_group", false)
end

function generator.all_utilities()
  -- there are no utility tags so these are a hardcoded selection of them
  local utilities = {type = "utility", tags = {}}

  for name, localised in pairs(utility_names) do
    table.insert(
      utilities.tags,
      {
        name = name,
        localized_name = localised -- TODO
      }
    )
  end

  return utilities
end

return generator
