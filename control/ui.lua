--[[
  ##############################
  Utility functions & constants
  ##############################
]]
local constants = require("ui-constants")

-- Splits given string
local function split(s, separator)
  local r = {}
  for token in string.gmatch(s, "[^" .. separator .. "]+") do
    if tonumber(token) ~= nil then
      token = tonumber(token)
    end
    table.insert(r, token)
  end
  return r
end

-- these functions are to find a number's sign and to round them
-- because lua doesn't have them baked in 😒
local function sign(v)
  return (v >= 0 and 1) or -1
end
local function round(v, bracket)
  bracket = bracket or 1
  return math.floor(v / bracket + sign(v) * 0.5) * bracket
end

local function to_rgb(color)
  return {r = round(color.r * 255), g = round(color.g * 255), b = round(color.b * 255)}
end

-- local function to_hsl(color)
--   return
-- end

-- gets the index of a LuaGuiElement on its parent.children[]
local function get_child_index(element)
  for index, name in pairs(element.parent.children_names) do
    if name == element.name then
      return index
    end
  end
end

-- find the main flow tab to whom the element is a child from
-- helps contextualize some stuff
local function find_main_flow(element)
  local main_flow = element
  repeat
    main_flow = main_flow.parent
  until string.find(main_flow.name, "^th_flow_tab_") or not element.parent

  --   for tags:
  -- main flow, search flow, tags scroll, nil
  --   for format:
  -- main flow, color frame, font frame, format flow
  return main_flow, main_flow.children[1], main_flow.children[2], main_flow.children[3]
end

local function find_enabled_output(dialog)
  local result = dialog.th_flow_output["th_textfield_output"]
  local big_result = dialog.th_flow_output["th_text-box_output"]

  if result.enabled then
    return result, big_result
  else
    return big_result, result
  end
end

-- format helper functions
-- finds the color swatch
local function find_swatch(color_frame)
  return color_frame.children[2].th_progressbar_swatch
end

-- finds the currently active slider field, and their tag
local function find_sliders(color_frame)
  local color_sliders_flow = color_frame.children[2].th_flow_color_sliders
  for key, child in pairs(color_sliders_flow.children) do
    if child.enabled then
      return child, split(child.name, "_")[4]
    end
  end
end

local function find_switches(font_frame)
  local switch_table = font_frame.children[2]
  return switch_table.children[2], switch_table.children[4]
end

-- finds the example text
local function find_format(main_flow)
  return main_flow.children[3].th_label_format
end

--[[
  ##############################
  Interface Building
  ##############################
]]
--[[
  ==============================
  Tags (emoji) tabs
  ==============================
]]
-- selects (and activates) the first visible group, if any
local function select_first_visible_group(tag_scroll)
  local tab_type = split(tag_scroll.name, "_")[4]
  local group_buttons = tag_scroll["th_table_groups_" .. tab_type]

  for _, gbutton in pairs(group_buttons.children) do
    local key = split(gbutton.name, "_")[5]
    local group = tag_scroll["th_flow_group_" .. tab_type .. "_" .. key]

    -- the first visible button (if any) gets selected!
    if gbutton.visible then
      gbutton.style = "th_button_tag_group_selected"
      group.visible = true
      return
    end
  end
end

-- resets a single tag to factory standards, visibility wise
local function reset_tag_scroll(tag_scroll)
  -- check if this is a tab with groups
  if #tag_scroll.children > 1 then
    -- and set the first as active
    for key, flow in pairs(tag_scroll.children) do
      if key == 1 then
        -- these are the groups
        for subkey, group in pairs(flow.children) do
          if subkey == 1 then
            group.style = "th_button_tag_group_selected"
          else
            group.style = "th_button_tag_group"
          end
          group.visible = true
        end
      else
        if key == 2 then
          -- this is the first group
          flow.visible = true
        else
          flow.visible = false
        end
        -- find all subgroups, then tags, then make them visible
        for _, subgroup in pairs(flow.children) do
          subgroup.visible = true
          for _, tag in pairs(subgroup.children) do
            tag.visible = true
          end
        end
      end
    end
    select_first_visible_group(tag_scroll)
  else
    -- find all subgroups, then tags, then make them visible
    local flow = tag_scroll.children[1]
    for _, subgroup in pairs(flow.children) do
      subgroup.visible = true
      for _, tag in pairs(subgroup.children) do
        tag.visible = true
      end
    end
  end
end

local function create_tags_tabs(player, tabbed_pane)
  -- (should be) tab-agnostic search bar + options
  local function create_search(parent_flow, type)
    local flow =
      parent_flow.add {
      type = "flow",
      direction = "horizontal",
      name = "th_flow_search_" .. type,
      style = "th_flow_search"
    }

    -- flow.add {type = }
    flow.add {
      type = "checkbox",
      name = "th_checkbox_img_" .. type,
      state = false,
      caption = {"th-captions.only-img"},
      tooltip = {"th-tooltips.only-img", {"th-tooltips.only-img-example-full"}, {"th-tooltips.only-img-example-img"}}
    }

    local subflow =
      flow.add {
      type = "flow",
      name = "th_flow_search_options_" .. type,
      style = "th_flow_search_options"
    }

    -- search
    subflow.add {
      type = "textfield",
      name = "th_textfield_search_" .. type,
      clear_and_focus_on_right_click = true,
      tooltip = {"th-tooltips.search"}
    }
  end

  local function create_tag_tab(type, is_grouped)
    local tab =
      tabbed_pane.add {
      type = "tab",
      name = "th_tab_tag_" .. type,
      style = "th_tab",
      caption = {"th-captions.tabs." .. type},
      tooltip = {"th-tooltips.tabs." .. type}
    }
    local flow =
      tabbed_pane.add {
      type = "flow",
      name = "th_flow_tab_tags_" .. type,
      style = "th_flow_tab",
      direction = "vertical"
    }
    tabbed_pane.add_tab(tab, flow)

    create_search(flow, type)

    local scroll =
      flow.add {
      name = "th_scroll_tags_" .. type,
      type = "scroll-pane",
      direction = "vertical",
      vertical_scroll_policy = "auto-and-reserve-space",
      style = "th_scroll_tags"
    }
    scroll.style.extra_left_margin_when_activated = 0
    scroll.style.extra_right_margin_when_activated = 0

    if is_grouped then
      local groups_table =
        scroll.add {
        type = "table",
        name = "th_table_groups_" .. type,
        style = "th_table_slots",
        column_count = 6
      }

      return scroll, groups_table
    else
      local default_flow =
        scroll.add {
        name = "th_flow_group_" .. type .. "_default",
        type = "flow",
        style = "th_flow_groups",
        direction = "vertical"
      }

      return scroll, default_flow
    end
  end

  -- creates a group within the "tab"
  local function create_group(scroll, groups_table, type, group)
    local name = type .. "_" .. group.name
    groups_table.add {
      type = "sprite-button",
      sprite = "item-group." .. group.name, -- sprites are always in item-group for some reason?
      name = "th_sprite-button_group_" .. name,
      tooltip = group.localised_name,
      style = "th_button_tag_group",
      mouse_button_filter = {
        "left"
      }
    }
    local subgroups_flow =
      scroll.add {
      name = "th_flow_group_" .. name,
      type = "flow",
      direction = "vertical",
      style = "th_flow_groups"
    }
    -- groups (as in, the subgroups's flow) start hidden by default
    -- we use a script to unhide the first one around when relevant
    subgroups_flow.visible = false

    return subgroups_flow
  end

  -- creates a button within the tabled(?) flow
  local function create_sprite_buttons(type, tags, flow)
    for _, tag in pairs(tags) do
      local sprite_path = type .. "." .. tag.name
      if not player.gui.is_valid_sprite_path(sprite_path) then
        -- kills off a few pesky... exceptions *cough* entities *cough*
        return
      end

      -- place the new button inside the subgroup (if it makes sense) table
      -- or inside a default "subgroup"
      local table_name
      if tag.subgroup then
        table_name = "th_table_subgroup_" .. type .. "_" .. tag.subgroup.name
      else
        table_name = "th_table_subgroup_" .. type .. "_default"
      end

      local table = flow[table_name]
      -- and if the table doesn't exist, create it
      if not table then
        table =
          flow.add {
          name = table_name,
          type = "table",
          column_count = 11,
          style = "th_table_slots"
        }
      end

      table.add {
        type = "sprite-button",
        tooltip = tag.localised_name,
        sprite = sprite_path,
        name = "th_sprite-button_tag_" .. sprite_path,
        style = "recipe_slot_button"
      }
    end
  end

  local item_scroll, item_groups_table = create_tag_tab("item", true)

  for _, items in pairs(global.all_items.groups) do
    local group = items[1].group
    local item_flow = create_group(item_scroll, item_groups_table, "item", group)
    create_sprite_buttons("item", items, item_flow)
  end

  for _, fluids in pairs(global.all_fluids.groups) do
    local group = fluids[1].group
    local fluid_flow = create_group(item_scroll, item_groups_table, "fluid", group)
    create_sprite_buttons("fluid", fluids, fluid_flow)
  end

  local signal_flow = create_group(item_scroll, item_groups_table, "virtual-signal", game.item_group_prototypes.signals)
  create_sprite_buttons("virtual-signal", global.all_signals.tags, signal_flow)

  --
  local _, tile_flow = create_tag_tab("tile")
  create_sprite_buttons("tile", global.all_tiles.tags, tile_flow)

  --
  local entity_scroll, entity_groups_table = create_tag_tab("entity", true)

  for _, entities in pairs(global.all_entities.groups) do
    local group = entities[1].group
    local entity_flow = create_group(entity_scroll, entity_groups_table, "entity", group)
    create_sprite_buttons("entity", entities, entity_flow)
  end

  --
  local _, group_flow = create_tag_tab("item-group")
  create_sprite_buttons("item-group", global.all_groups.tags, group_flow)

  --
  local recipe_scroll, recipe_groups_table = create_tag_tab("recipe", true)

  for _, recipes in pairs(global.all_recipes.groups) do
    local group = recipes[1].group
    local recipe_flow = create_group(recipe_scroll, recipe_groups_table, "recipe", group)
    create_sprite_buttons("recipe", recipes, recipe_flow)
  end

  --
  local _, technology_flow = create_tag_tab("technology")
  create_sprite_buttons("technology", global.all_technologies.tags, technology_flow)

  --
  local _, achievement_flow = create_tag_tab("achievement")
  create_sprite_buttons("achievement", global.all_achievements.tags, achievement_flow)

  --
  local _, equipment_flow = create_tag_tab("equipment")
  create_sprite_buttons("equipment", global.all_equipment.tags, equipment_flow)

  --
  local _, utility_flow = create_tag_tab("utility")
  create_sprite_buttons("utility", global.all_utilities.tags, utility_flow)
end

--[[
  ==============================
  Formatting tab
  ==============================
]]
-- local function sync_sliders(sliders)
--   sliders.children[3].text = sliders.children[2].slider_value
--   sliders.children[6].text = sliders.children[5].slider_value
--   sliders.children[9].text = sliders.children[8].slider_value
-- end

local function get_color_from_sliders(sliders, type)
  local color = {r = 0, g = 0, b = 0}
  if type == "rgb" then
    -- this does the rgbb -> decimal conversion
    color.r = sliders.children[2].slider_value / 255
    color.g = sliders.children[5].slider_value / 255
    color.b = sliders.children[8].slider_value / 255
  elseif type == "hsl" then
    -- local hsl_color = to_hsl(color)
    return
  end
  return color
end

local function set_sliders(sliders, type, color)
  if type == "rgb" then
    -- this does the decimal -> rgb conversion
    local rgb_color = to_rgb(color)
    sliders.children[2].slider_value = rgb_color.r
    sliders.children[3].text = rgb_color.r
    sliders.children[5].slider_value = rgb_color.g
    sliders.children[6].text = rgb_color.g
    sliders.children[8].slider_value = rgb_color.b
    sliders.children[9].text = rgb_color.b
  elseif type == "hsl" then
    -- local hsl_color = to_hsl(color)
    return
  end
end

local function update_color_from_swatch(swatch)
  local _, color_frame = find_main_flow(swatch)
  local color = swatch.style.color

  -- update sliders
  local sliders, type = find_sliders(color_frame)
  set_sliders(sliders, type, color)

  -- update... hex? TODO

  -- update format text
  local format = find_format(color_frame.parent)
  format.style.font_color = color
end

local function update_color_from_sliders(sliders)
  -- bit hackish but as we're already inside the sliders, we know they're active
  -- future me, feel free to foolproof with find_sliders
  local type = split(sliders.name, "_")[4]
  local color = get_color_from_sliders(sliders, type)

  -- set swatch to color
  local _, color_frame = find_main_flow(sliders)
  local swatch = find_swatch(color_frame)
  swatch.style.color = color

  -- reset preset to Default
  color_frame.th_flow_format_preset_color.children[1].selected_index = 1

  -- update... hex? TODO

  -- update format text
  local format = find_format(color_frame.parent)
  format.style.font_color = color
end

local function get_font(font_frame, index)
  if not index then
    local font_drop = font_frame.th_flow_format_preset_font.children[1]
    index = font_drop.selected_index
  end

  local font = "default"
  if index == 0 then
    return font
  elseif index == 1 then
    -- default, need to read the swatches
    local size_switch, weight_switch = find_switches(font_frame)
    local size = size_switch.switch_state
    if size == "left" then
      -- small
      font = font .. "-small"
    elseif size == "right" then
      -- big
      font = font .. "-large"
    end
    local weight = weight_switch.switch_state
    if weight == "left" then
      -- semibold
      font = font .. "-semibold"
    elseif weight == "right" then
      -- bold
      font = font .. "-bold"
    end
    return font
  else
    return constants.font_presets[index - 1]
  end
end

local function update_format_font(font_frame)
  local index = font_frame.th_flow_format_preset_font.children[1].selected_index

  local format = find_format(font_frame.parent, index)
  format.style.font = get_font(font_frame)
end

local function create_formatting_tab(tabbed_pane)
  local function create_preset_flow(frame, presets, type)
    local preset_flow =
      frame.add {
      type = "flow",
      name = "th_flow_format_preset_" .. type
    }

    preset_flow.add {
      type = "drop-down",
      items = presets,
      name = "th_drop-down_preset_" .. type,
      selected_index = 1
    }

    local submit_alignment =
      preset_flow.add {
      type = "flow"
    }
    submit_alignment.style.horizontally_stretchable = true
    submit_alignment.style.horizontal_align = "right"

    -- it's aliged by the right, so stuff is added from the right to the left!
    submit_alignment.add {
      type = "sprite-button",
      name = "th_sprite-button_scrub_" .. type,
      sprite = "utility.clear",
      style = "red_icon_button",
      tooltip = {"th-tooltips.scrub-" .. type}
    }

    submit_alignment.add {
      type = "button",
      name = "th_button_format_" .. type,
      caption = {"th-captions.format-" .. type},
      tooltip = {"th-tooltips.format-" .. type}
    }
  end

  -- slider for rgb/hsl/...
  local function create_color_slider(table, type)
    table.add {
      type = "label",
      caption = {"th-captions.color-slider." .. type},
      tooltip = {"th-tooltips.color-slider." .. type}
    }

    table.add {
      type = "slider",
      style = "th_slider_color_" .. type,
      name = "th_slider_color_" .. type,
      minimum_value = 0,
      maximum_value = 255,
      discrete_slider = true,
      discrete_values = true,
      value_step = 1
    }

    table.add {
      type = "textfield",
      name = "th_textfield_color_" .. type,
      style = "th_textfield_color_slider_value",
      numeric = true,
      allow_decimal = false,
      allow_negative = false
    }
  end

  local tab =
    tabbed_pane.add {
    type = "tab",
    name = "th_tab_format",
    caption = {"th-captions.tabs.format"},
    tooltip = {"th-tooltips.tabs.format"}
  }
  tab.style.minimal_width = 32
  local flow =
    tabbed_pane.add {
    type = "flow",
    name = "th_flow_tab_format",
    direction = "vertical"
  }
  flow.style.left_padding = 8
  flow.style.right_padding = 8
  flow.style.horizontal_align = "center"

  tabbed_pane.add_tab(tab, flow)

  ---[[ Color ]]

  local color_frame =
    flow.add {
    type = "frame",
    name = "th_frame_color",
    style = "bordered_frame",
    caption = {"th-captions.color-frame"},
    direction = "vertical"
  }
  color_frame.style.left_margin = -1
  color_frame.style.right_margin = -1
  color_frame.style.use_header_filler = false

  create_preset_flow(color_frame, constants.color_items, "color")

  local color_table =
    color_frame.add {
    type = "table",
    column_count = 2
  }
  color_table.style.vertical_align = "center"

  color_table.add {
    type = "progressbar",
    style = "th_progressbar_swatch",
    name = "th_progressbar_swatch",
    value = 1
  }

  local color_slider_flow =
    color_table.add {
    type = "flow",
    direction = "vertical",
    name = "th_flow_color_sliders"
  }

  local rgb_slider_table =
    color_slider_flow.add {
    type = "table",
    direction = "vertical",
    column_count = 3,
    name = "th_table_slider_rgb"
  }

  create_color_slider(rgb_slider_table, "r", "R")
  create_color_slider(rgb_slider_table, "g", "G")
  create_color_slider(rgb_slider_table, "b", "B")

  local hsl_slider_table =
    color_slider_flow.add {
    type = "table",
    direction = "vertical",
    column_count = 3,
    name = "th_table_slider_hsl"
  }

  create_color_slider(hsl_slider_table, "h", "H")
  create_color_slider(hsl_slider_table, "s", "S")
  create_color_slider(hsl_slider_table, "l", "L")

  hsl_slider_table.enabled = false
  hsl_slider_table.visible = false

  local slider_scheme =
    color_table.add {
    type = "sprite-button",
    sprite = "th_rgb",
    style = "th_button_color_scheme",
    name = "th_sprite-button_color_scheme",
    tooltip = {"th-tooltips.rgb"}
  }
  slider_scheme.enabled = false

  local hex_flow =
    color_table.add {
    type = "flow"
  }
  hex_flow.style.vertical_align = "center"

  local hex_label =
    hex_flow.add {
    type = "label",
    caption = {"th-captions.hex"}
  }
  hex_label.enabled = false

  local hex =
    hex_flow.add {
    type = "textfield",
    name = "th_textfield_hex",
    tooltip = {"th-tooltips.hex"}
  }
  hex.enabled = false

  --[[ Font ]]
  local font_frame =
    flow.add {
    type = "frame",
    name = "th_frame_font",
    style = "bordered_frame",
    caption = {"th-captions.font-frame"},
    direction = "vertical"
  }
  -- font_frame.style.width = ((68 + 1) * 6) + 19
  font_frame.style.left_margin = -1
  font_frame.style.right_margin = -1
  font_frame.style.use_header_filler = false

  create_preset_flow(font_frame, constants.font_items, "font")

  local font_table =
    font_frame.add {
    type = "table",
    column_count = 2
  }
  -- font_table.style.horizontally_stretchable = true

  font_table.add {type = "label", caption = {"th-captions.font-size"}}

  font_table.add {
    type = "switch",
    allow_none_state = true,
    switch_state = "none",
    name = "th_switch_size",
    left_label_caption = {"th-captions.font-small"},
    right_label_caption = {"th-captions.font-large"}
  }

  font_table.add {type = "label", caption = {"th-captions.font-weight"}}

  font_table.add {
    type = "switch",
    allow_none_state = true,
    switch_state = "none",
    name = "th_switch_weight",
    left_label_caption = {"th-captions.font-semibold"},
    -- left_label_tooltip :: LocalisedString (optional)
    right_label_caption = {"th-captions.font-bold"}
    -- right_label_tooltip :: LocalisedString (optional)
  }

  -- the test label, with a flow around it to center it vertically
  local format_flow =
    flow.add {
    type = "flow",
    direction = "vertical"
  }
  format_flow.style.vertically_stretchable = true
  format_flow.style.vertical_align = "center"

  format_flow.add {
    type = "label",
    name = "th_label_format",
    caption = {"th-captions.format"}
  }
end

--[[
  ==============================
  Main function
  ==============================
]]
-- refreshes the main display
-- full_refresh, if true, recreates the whole display from scratch
local function refresh_main_dialog(player, full_refresh)
  local main_dialog = player.gui.screen["th_frame_main_dialog"]
  -- local categories = global.categories
  -- log(serpent.block(categories))

  if full_refresh then
    -------------
    if main_dialog == nil then
      main_dialog =
        player.gui.screen.add {
        type = "frame",
        name = "th_frame_main_dialog",
        direction = "vertical",
        caption = {"th-captions.main-frame"}
      }
    end
    main_dialog.clear()

    --[[
      Tabs
    ]]
    local tabbed_pane = main_dialog.add {type = "tabbed-pane", name = "th_tabbed-pane"}

    create_tags_tabs(player, tabbed_pane)
    create_formatting_tab(tabbed_pane)

    --[[
      Output
    ]]
    local output_flow = main_dialog.add {type = "flow", name = "th_flow_output"}

    local output = output_flow.add {type = "textfield", name = "th_textfield_output", style = "th_textbox_output_small"}
    output.visible = true

    local big_output = output_flow.add {type = "text-box", name = "th_text-box_output", style = "th_textbox_output_big"}
    big_output.word_wrap = true
    big_output.selectable = true
    big_output.visible = false
    big_output.enabled = false

    output_flow.add {
      type = "sprite-button",
      sprite = "utility.expand",
      name = "th_sprite-button_expand_output",
      style = "th_button_output_expand",
      tooltip = {"th-tooltips.expand-output"},
      mouse_button_filter = {
        "left"
      }
    }

    -- the tag scrolls are freshly baked, so resetting them is silly
    -- but we still need to select the first group on them all
    for type, info in pairs(constants.tab_types) do
      if not info.subtype then
        local tag_scroll = tabbed_pane["th_flow_tab_tags_" .. type].children[2]
        -- if it has groups, that is
        if #tag_scroll.children > 1 then
          select_first_visible_group(tag_scroll)
        end
      end
    end
  else
    local tabbed_pane = main_dialog["th_tabbed-pane"]

    for type, info in pairs(constants.tab_types) do
      if not info.subtype then
        local tab = tabbed_pane["th_flow_tab_tags_" .. type]
        -- clears (and enables) the search bar
        tab.children[1].children[2].children[1].text = ""
        tab.children[1].children[2].children[1].enabled = true
        -- make all recipes visible (in case of search mishap)
        reset_tag_scroll(tab.children[2])
      end
    end
  end

  -- also need to set a default color (orange)
  local swatch = find_swatch(main_dialog["th_tabbed-pane"].th_flow_tab_format.th_frame_color)
  swatch.style.color = constants.color_presets.orange
  update_color_from_swatch(swatch)

  main_dialog.force_auto_center()

  return main_dialog
end

--[[
  ##############################
  Search
  ##############################
]]
-- returns the first valid search query on the tabbed plane
-- or returns a "" to reset the current tab
local function find_valid_search_query(tabbed_pane)
  for key, type in pairs(constants.tab_types) do
    if not type.subtype then
      local search_flow = tabbed_pane["th_flow_tab_tags_" .. key].children[1]
      local search = search_flow.children[2].children[1]
      if search.enabled then
        return search.text
      end
    end
  end
  return ""
end

-- applies the filter on the items
-- heavily based on factory planner's strategy
local function search_query(tag_scroll, query)
  -- clears whitespace
  local search_term = query:gsub("^%s*(.-)%s*$", "%1")
  -- and inside it for added measure
  search_term = search_term:gsub("%s*", "")

  if search_term == "" then
    -- clean the filter if it's empty
    reset_tag_scroll(tag_scroll)
    return
  end

  local total = 0

  local group_buttons = nil
  local groups = {}
  if #tag_scroll.children > 1 then
    group_buttons = tag_scroll.children[1]
    for key, flow in pairs(tag_scroll.children) do
      if key ~= 1 then
        table.insert(groups, flow)
      end
    end
  else
    table.insert(groups, tag_scroll.children[1])
  end

  -- negative logic: start everything invisible, turn visible only what matches
  local any_selected_group = false
  for key, group in pairs(groups) do
    local group_visible = false

    for _, subgroup in pairs(group.children) do
      local subgroup_visible = false

      for _, tag in pairs(subgroup.children) do
        local visible = false

        -- the actual check of a filter
        -- remove the whitespace (and '-') on here too
        -- TODO: use player:request translation
        -- and make this truly multilingual.
        local name = split(tag.name, ".")[2]:gsub("%s*", ""):gsub("%-*", "")

        if string.find(name, search_term, 1, true) then
          visible = true
        end

        tag.visible = visible
        if visible then
          subgroup_visible = true
          group_visible = true
          total = total + 1
        end
      end

      subgroup.visible = subgroup_visible
    end

    if group_buttons then
      local group_button = group_buttons.children[key]
      group_button.visible = group_visible

      -- two checks being done here: if the selected group should stay selected
      if group.visible then
        if not group_visible then
          group.visible = false
          -- deselect the button too while at it
          group_button.style = "th_button_tag_group"
        else
          any_selected_group = true
        end
      end
    else
      -- no groups means its group is always selected!
      any_selected_group = true
    end
  end

  -- gotta reset the selected group after a search if we deselected it
  if not any_selected_group then
    select_first_visible_group(tag_scroll)
  end
end

--[[
  ##############################
  Frame event listeners
  ##############################
]]
-- TODO: read selection?
-- script.on_event(
--   "th-quick-format",
--   function(event)
--     local player = game.get_player(event.player_index)

--     local screen = player.gui.screen
--     local main_dialog = screen["th_frame_main_dialog"]
--     if main_dialog then
--       local output
--     end
--     toggle_main_dialog(player)
--   end
-- )

-- TODO: shortcut?
-- script.on_event(
--   defines.events.on_lua_shortcut,
--   function(event)
--     local player = game.get_player(event.player_index)

--     if event.prototype_name == "tag-helper" then
--       toggle_main_dialog(player)
--     end
--   end
-- )

-- toggles main display visibility, and rebuilds it if need be
local function toggle_main_dialog(player)
  local screen = player.gui.screen
  local main_dialog = screen["th_frame_main_dialog"]

  local open

  -- Create and open main dialog, if it doesn't exist yet
  if main_dialog == nil then
    -- Otherwise, toggle it
    main_dialog = refresh_main_dialog(player, true)
    open = true
  else
    if main_dialog.visible then
      open = false
    else
      -- Only refresh it when you make it visible
      main_dialog = refresh_main_dialog(player)
      open = true
    end
  end

  main_dialog.visible = open
  player.opened = open and main_dialog or nil
end

-- toggle main tag helper window
script.on_event(
  "tag-helper",
  function(event)
    local player = game.get_player(event.player_index)
    toggle_main_dialog(player)
  end
)

-- Focuses on the searchbox as with a vanilla recipe screen
script.on_event(
  "th-focus-search",
  function(event)
    local player = game.get_player(event.player_index)
    local main_dialog = player.gui.screen.th_frame_main_dialog
    if main_dialog and main_dialog.visible then
      local tabs = main_dialog["th_tabbed-pane"]
      -- if no tab is selected, the first is selected by default
      local main_flow = tabs.tabs[tabs.selected_tab_index or 1].content
      if main_flow and string.find(main_flow.name, "^th_flow_tab_tags") then
        local searchbox = main_flow.children[1].children[2].children[1]
        if searchbox.enabled then
          searchbox.focus()
        end
      end
    end
  end
)

-- Fires the user action of closing a dialog (Q/Esc/...)
script.on_event(
  defines.events.on_gui_closed,
  function(event)
    local player = game.get_player(event.player_index)

    if
      event.gui_type == defines.gui_type.custom and event.element and event.element.visible and
        string.find(event.element.name, "^th_")
     then
      toggle_main_dialog(player)
    end
  end
)

--[[
  ##############################
  Element agnostic events
  ##############################
  ]]
-- TODO: shortcut?
-- script.on_event(
--   defines.events.on_lua_shortcut,
--   function(event)
--     local player = game.get_player(event.player_index)

--     if event.prototype_name == "tag-helper" then
--       toggle_main_dialog(player)
--     end
--   end
-- )

local function on_scrub_color(dialog)
  local output = find_enabled_output(dialog)
  -- TODO: do proper tree detection (?)
  output.text = string.gsub(output.text, "^.-%[color=.-%](.*)%[/color%].-$", "%1", 1)
end

-- formats the *whole* output box with the specified color
local function on_format_color(dialog, color_frame, control)
  local dropdown = color_frame.th_flow_format_preset_color.children[1]
  local index = dropdown.selected_index
  local color_text
  if index == 1 then
    -- if the preset's at default, use the swatch's color
    local swatch = find_swatch(color_frame)
    local color = swatch.style.color
    -- todo: set output type? :|
    local rgb_color = to_rgb(color)
    color_text = "[color=" .. rgb_color.r .. "," .. rgb_color.g .. "," .. rgb_color.b .. "]"
  else
    -- use the preset's name directly
    local name = split(dropdown.get_item(index)[4][1], ".")[2]
    color_text = "[color=" .. name .. "]"
  end

  local output = find_enabled_output(dialog)
  -- if in control mode, surround first smallest <text> with color info
  if control then
    if string.find(output.text, "<") then
      if string.find(output.text, ">") then
        output.text = string.gsub(output.text, "<([^<>]-)>", color_text .. "%1[/color]", 1)
      else
        output.text = string.gsub(output.text, "<", color_text, 1) .. "[/color]"
      end
      return
    end
  end
  output.text = color_text .. output.text .. "[/color]"
end

local function on_scrub_font(dialog)
  local output = find_enabled_output(dialog)
  -- TODO: do proper tree detection (?)
  output.text = string.gsub(output.text, "^.-%[font=.-%](.*)%[/font%].-$", "%1", 1)
end

-- formats the *whole* output box with the specified font
local function on_format_font(dialog, font_frame)
  local font = get_font(font_frame)

  local output = find_enabled_output(dialog)
  output.text = "[font=" .. font .. "]" .. output.text .. "[/font]"
end

-- toggles between an expanded (rich text off) and simple (on) text box
local function on_expand_output_toggle(dialog, button)
  local result = dialog.th_flow_output["th_textfield_output"]
  local big_result = dialog.th_flow_output["th_text-box_output"]

  if result.enabled then
    result.enabled = false
    result.visible = false
    big_result.text = result.text
    big_result.enabled = true
    big_result.visible = true
    button.style = "th_button_output_expand_selected"
    button.tooltip = {"th-tooltips.shrink-output"}
    button.sprite = "utility.collapse"
  else
    big_result.enabled = false
    big_result.visible = false
    result.text = big_result.text
    result.enabled = true
    result.visible = true
    button.style = "th_button_output_expand"
    button.tooltip = {"th-tooltips.expand-output"}
    button.sprite = "utility.expand"
  end
end

-- selects (makes visible) a tag group
local function on_group_select(tag_scroll, button)
  -- tab type isn't always the sprite's type
  -- (see: fluids sharing the item tab)

  -- local tab_type = split(tag_scroll.name, "_")[4]
  local group_buttons = tag_scroll.children[1]

  for i, gbutton in ipairs(group_buttons.children) do
    local group = tag_scroll.children[i + 1]

    if gbutton == button then
      -- this was the clicked-on button!
      -- so turn this on and make its group visible
      gbutton.style = "th_button_tag_group_selected"
      group.visible = true
    else
      -- turn/hide this one off
      gbutton.style = "th_button_tag_group"
      group.visible = false
    end
  end
end

local function on_tag_select(dialog, search_flow, sprite_path, click, control)
  local split_path = split(sprite_path, ".")
  local type = split_path[1]
  local tag = split_path[2]
  -- tab type isn't always the sprite's type (see: fluids sharing the item tab)
  -- local tab_type = split(search_flow.name, "_")[4]

  local is_img = search_flow.children[1].state
  if click == "right" then
    is_img = not is_img
  end

  local sprite
  if is_img or constants.tab_types[type].img_only then
    sprite = "[img=" .. sprite_path .. "]"
  else
    sprite = "[" .. type .. "=" .. tag .. "]"
  end

  local output = find_enabled_output(dialog)
  -- if in control, replace the first @ with the tag
  if control then
    if string.find(output.text, "@") then
      output.text = string.gsub(output.text, "@", sprite, 1)
      return
    end
  end
  output.text = output.text .. sprite
end

-- Fires on any click on a GUI element
script.on_event(
  defines.events.on_gui_click,
  function(event)
    local element = event.element
    local player = game.get_player(event.player_index)
    local click = nil

    if event.button == defines.mouse_button_type.left then
      click = "left"
    elseif event.button == defines.mouse_button_type.right then
      click = "right"
    end

    if string.find(element.name, "^th_") then
      local dialog = player.gui.screen.th_frame_main_dialog

      if element.name == "th_sprite-button_scrub_color" then
        -- removes the outermost color []
        on_scrub_color(dialog)
      elseif element.name == "th_button_format_color" then
        -- applies the color formatting
        local _, color_frame = find_main_flow(element)
        on_format_color(dialog, color_frame, event.control)
      elseif element.name == "th_sprite-button_scrub_font" then
        -- removes the outermost font []
        on_scrub_font(dialog)
      elseif element.name == "th_button_format_font" then
        -- applies the font formatting
        local _, _, font_frame = find_main_flow(element)
        on_format_font(dialog, font_frame, event.control)
      elseif element.name == "th_sprite-button_expand_output" then
        -- expand output button
        on_expand_output_toggle(dialog, element)
      elseif string.find(element.name, "^th_sprite%-button_tag_") then
        -- any sprited tag button
        local _, search_scroll = find_main_flow(element)
        on_tag_select(dialog, search_scroll, element.sprite, click, event.control)
      elseif string.find(element.name, "^th_sprite%-button_group_") then
        -- tag group button
        local _, _, tag_scroll = find_main_flow(element)
        on_group_select(tag_scroll, element)
      end
    end
  end
)

--[[
  ##############################
  Element specific events
  ##############################
  ]]
-- syncs image checkboxes on all tabs
local function sync_img_option(tabbed_pane, checkbox)
  local state = checkbox.state

  for key, info in pairs(constants.tab_types) do
    if not info.subtype then
      local search_flow = tabbed_pane["th_flow_tab_tags_" .. key].children[1]
      local box = search_flow.children[1]
      if box ~= checkbox then
        box.state = state
      end
    end
  end
end

-- Fires on any radio-button change
script.on_event(
  defines.events.on_gui_checked_state_changed,
  function(event)
    local element = event.element
    local player = game.get_player(event.player_index)

    if string.find(element.name, "^th_checkbox_img") then
      local tabbed_pane = player.gui.screen.th_frame_main_dialog["th_tabbed-pane"]
      sync_img_option(tabbed_pane, element)
    end
  end
)

-- Fires on any confirmation of a textfield
-- script.on_event(
--   defines.events.on_gui_confirmed,
--   function(_)
--     -- local element = event.element
--     -- local player = game.get_player(event.player_index)
--     -- game.print("GUI CONFIRMED")
--   end
-- )

-- Fires the user action of changing the selected tab
script.on_event(
  defines.events.on_gui_selected_tab_changed,
  function(event)
    local element = event.element

    if element.name == "th_tabbed-pane" and element.selected_tab_index then
      local main_flow = element.tabs[element.selected_tab_index].content
      if string.find(main_flow.name, "^th_flow_tab_tags") then
        local search_flow = main_flow.children[1]
        local searchbox = search_flow.children[2].children[1]
        if not searchbox.enabled then
          local query = find_valid_search_query(main_flow.parent)
          -- only refresh if the local search's disabled and has different contents
          if query ~= searchbox.text then
            local tag_scroll = main_flow.children[2]
            search_query(tag_scroll, query)
            searchbox.text = query
          end
          searchbox.enabled = true
        end

        -- deselect the format tab while at it
        element.th_tab_format.caption = {"th-captions.tabs.format"}
      else
        -- it's the format tab, make it selected
        element.th_tab_format.caption = {"th-captions.tabs.format-selected"}
      end
    end
  end
)

local function on_color_preset(color_frame, color)
  -- set swatch to color
  local swatch = find_swatch(color_frame)
  swatch.style.color = color

  update_color_from_swatch(swatch)
end

-- Fires on any selection change on list/dropboxes
script.on_event(
  defines.events.on_gui_selection_state_changed,
  function(event)
    local element = event.element
    local index = element.selected_index

    if index <= 0 then
      return
    end

    if element.name == "th_drop-down_preset_color" then
      -- default do nothing when selected
      if index == 1 then
        return
      end

      -- color_presets is metatable'd so indexes also work on it
      local color = constants.color_presets[index - 1]
      local _, color_frame = find_main_flow(element)
      on_color_preset(color_frame, color)
    elseif element.name == "th_drop-down_preset_font" then
      local _, _, font_frame = find_main_flow(element)
      -- if default, enable switches, otherwise disable
      local enabled = index == 1

      for _, child in pairs(font_frame.children[2].children) do
        child.enabled = enabled
      end

      -- update text accordingly
      update_format_font(font_frame)
    end
  end
)

script.on_event(
  defines.events.on_gui_switch_state_changed,
  function(event)
    local element = event.element

    if string.find(element.name, "^th_switch") then
      local _, _, font_frame = find_main_flow(element)
      update_format_font(font_frame)
    end
  end
)

-- syncs the slider with its caption box (if valid)
local function on_color_field_change(textfield)
  local value = tonumber(textfield.text)

  -- sliders are always an index behind their textfield
  local slider = textfield.parent.children[get_child_index(textfield) - 1]
  -- don't do anything if the value's too big, let it correct itself later
  -- TODO: this probably won't do outside of RGB
  if not value then
    value = 0
  elseif value > 255 then
    return
  end
  slider.slider_value = value

  update_color_from_sliders(textfield.parent)
end

-- invalidates all search boxes that aren't visible (on another tab)
-- so when swapping to that tab it's refreshed with a search
local function sync_searchbox(tabbed_pane, searchbox)
  for key, info in pairs(constants.tab_types) do
    if not info.subtype then
      local search_flow = tabbed_pane["th_flow_tab_tags_" .. key].children[1]
      local search = search_flow.children[2].children[1]
      if search ~= searchbox then
        search.enabled = false
      end
    end
  end
end

-- Fires on any changes to a textbox/-field
script.on_event(
  defines.events.on_gui_text_changed,
  function(event)
    local element = event.element

    if string.find(element.name, "^th_textfield_color_") then
      on_color_field_change(element)
    elseif string.find(element.name, "^th_textfield_search_") then
      local main_flow, _, tag_scroll = find_main_flow(element)
      -- print("search " .. element.text)
      sync_searchbox(main_flow.parent, element)
      search_query(tag_scroll, element.text)
    end
  end
)

-- syncs the slider with its caption box (if valid)
local function on_color_slider_change(slider)
  local value = slider.slider_value

  -- a slider's textfield is always one index forward
  local textfield = slider.parent.children[get_child_index(slider) + 1]
  textfield.text = value

  update_color_from_sliders(slider.parent)
end

-- Fires on changes to a slider value change
script.on_event(
  defines.events.on_gui_value_changed,
  function(event)
    local element = event.element

    if string.find(element.name, "^th_slider_color_") then
      on_color_slider_change(element)
    end
  end
)
