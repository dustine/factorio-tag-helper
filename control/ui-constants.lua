local constants = {}

constants.tab_types = {
  item = {has_groups = true},
  fluid = {subtype = "item", has_groups = true},
  ["virtual-signal"] = {subtype = "item", has_groups = false},
  tile = {has_groups = false},
  entity = {has_groups = true},
  ["item-group"] = {has_groups = false},
  recipe = {has_groups = true},
  technology = {has_groups = false},
  achievement = {has_groups = false},
  equipment = {has_groups = false, img_only = true},
  utility = {has_groups = false, img_only = true}
}

constants.tab_type_order = {
  "item",
  "fluid",
  "virtual-signal",
  "tile",
  "entity",
  "item-group",
  "recipe",
  "technology",
  "achievement",
  "equipment",
  "utility"
}

constants.color_presets = {
  red = {r = 1.000, g = 0.166, b = 0.141},
  green = {r = 0.173, g = 0.824, b = 0.250},
  blue = {r = 0.343, g = 0.683, b = 1.000},
  orange = {r = 1.000, g = 0.630, b = 0.259},
  yellow = {r = 1.000, g = 0.828, b = 0.231},
  pink = {r = 1.000, g = 0.520, b = 0.633},
  purple = {r = 0.821, g = 0.440, b = 0.998},
  white = {r = 0.9, g = 0.9, b = 0.9},
  black = {r = 0.5, g = 0.5, b = 0.5},
  gray = {r = 0.7, g = 0.7, b = 0.7},
  brown = {r = 0.757, g = 0.522, b = 0.371},
  cyan = {r = 0.335, g = 0.918, b = 0.866},
  acid = {r = 0.708, g = 0.996, b = 0.134}
}
-- set metatables so the table also works to fetch indexes
-- probably not recommended for pairs() iteration!
local color_indexed = {}
for _, color in pairs(constants.color_presets) do
  table.insert(color_indexed, color)
end

setmetatable(
  color_indexed,
  {
    __index = function()
      return constants.color_presets.orange
    end
  }
)
setmetatable(constants.color_presets, {__index = color_indexed})

constants.color_items = {{"th-colors.default"}}
for name, _ in pairs(constants.color_presets) do
  table.insert(
    constants.color_items,
    {
      "",
      "[color=" .. name .. "]",
      {"th-colors." .. name},
      "[/color]"
    }
  )
end

constants.font_presets = {
  "compi",
  "compilatron-message-font",
  "count-font",
  "default-dialog-button",
  "default-dropdown",
  "default-game",
  "default-listbox",
  "default-tiny-bold",
  "locale-pick",
  "heading-1",
  "heading-2",
  "heading-3",
  "scenario-message-dialog",
  "technology-slot-level-font",
  "var"
}
constants.font_items = {{"th-fonts.default"}}
for _, font in pairs(constants.font_presets) do
  table.insert(
    constants.font_items,
    {
      "th-fonts." .. font
    }
  )
end

return constants
