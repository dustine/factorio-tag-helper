data:extend {
  {
    type = "sprite",
    name = "th_rgb",
    filename = "__tag-helper__/graphics/rgb.png",
    priority = "extra-high-no-scale",
    width = 64,
    height = 64
  },
  {
    type = "sprite",
    name = "th_hsl",
    filename = "__tag-helper__/graphics/hsl.png",
    priority = "extra-high-no-scale",
    width = 64,
    height = 64
  }
}
