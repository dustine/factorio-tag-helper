data:extend {
  {
    type = "custom-input",
    name = "tag-helper",
    key_sequence = "ALT + PERIOD",
    consuming = "none" --'game-only'
  },
  {
    type = "custom-input",
    name = "th-focus-search",
    key_sequence = "",
    linked_game_control = "focus-search"
  }
}
