local function add_style(name, style)
  data.raw["gui-style"].default[name] = style
end

-- [[Common]]

-- six group slot buttons, with 1px padding inside
local magical_number = (68 + 1) * 6 - 1

add_style(
  "th_tab",
  {
    type = "tab_style",
    parent = "tab",
    minimal_width = 32
  }
)

add_style(
  "th_flow_tab",
  {
    type = "vertical_flow_style",
    left_padding = 8,
    right_padding = 8,
    natural_width = magical_number
  }
)

-- [[Format]]
add_style(
  "th_progressbar_swatch",
  {
    type = "progressbar_style",
    bar_width = 96,
    -- bar_width = 120,
    width = 60
    -- height = 64
  }
)
add_style(
  "th_textfield_color_slider_value",
  {
    type = "textbox_style",
    parent = "slider_value_textfield",
    width = 50,
    minimal_height = 20
  }
)

add_style(
  "th_button_color_scheme",
  {
    type = "button_style",
    parent = "icon_button",
    size = 28,
    padding = 2
  }
)

-- [[ Sliders ]]
add_style(
  "th_slider_color_r",
  {
    type = "slider_style",
    parent = "red_slider",
    maximal_width = 0,
    horizontally_stretchable = "on"
  }
)
add_style(
  "th_slider_color_g",
  {
    type = "slider_style",
    parent = "green_slider",
    maximal_width = 0,
    horizontally_stretchable = "on"
  }
)
add_style(
  "th_slider_color_b",
  {
    type = "slider_style",
    parent = "blue_slider",
    maximal_width = 0,
    horizontally_stretchable = "on"
  }
)
add_style(
  "th_slider_color_h",
  {
    type = "slider_style",
    parent = "slider",
    maximal_width = 0,
    horizontally_stretchable = "on"
  }
)
add_style(
  "th_slider_color_s",
  {
    type = "slider_style",
    parent = "slider",
    maximal_width = 0,
    horizontally_stretchable = "on"
  }
)
add_style(
  "th_slider_color_l",
  {
    type = "slider_style",
    parent = "slider",
    maximal_width = 0,
    horizontally_stretchable = "on"
  }
)

-- [[ Tag ]]

-- data.raw["gui-style"].default.dth_scroll_pane_single_tab = {
--   type = "scroll_pane_style",
--   maximal_height = 36 * (10 + 3),
--   parent = "scroll_pane"
-- }
-- data.raw["gui-style"].default.dth_scroll_pane_multiple_tab = {
--   type = "scroll_pane_style",
--   maximal_height = 36 * (10 + 3),
--   parent = "scroll_pane"
-- }

add_style(
  "th_flow_search",
  {
    type = "horizontal_flow_style",
    vertical_align = "center",
    bottom_margin = 4
  }
)

add_style(
  "th_flow_search_options",
  {
    type = "horizontal_flow_style",
    horizontal_align = "right",
    maximum_width = 0,
    horizontally_stretchable = "on"
  }
)

add_style(
  "th_scroll_tags",
  {
    -- parent = "scroll_pane"
    type = "scroll_pane_style",
    parent = "tab_scroll_pane",
    width = magical_number + 20, -- TODO: redo?
    height = ((36 + 1) * 10) - 1 - 1, -- TODO: redo??,
    extra_left_margin_when_activated = 0,
    extra_right_margin_when_activated = 0
  }
)

add_style(
  "th_flow_groups",
  {
    type = "vertical_flow_style",
    vertical_spacing = 1
  }
)

add_style(
  "th_table_slots",
  {
    type = "table_style",
    horizontal_spacing = 1,
    vertical_spacing = 1
  }
)

-- buttons
add_style(
  "th_button_tag",
  {
    type = "button_style",
    parent = "slot_button"
  }
)
add_style(
  "th_button_tag_group",
  {
    type = "button_style",
    parent = "image_tab_slot"
  }
)
add_style(
  "th_button_tag_group_selected",
  {
    type = "button_style",
    parent = "image_tab_selected_slot"
  }
)

-- set the unselected group button with the sam graphic for when clicked on as when it's selected
-- blends better, as it avoids the click jumping back to grey
local tab_base = data.raw["gui-style"].default.th_button_tag_group
tab_base.clicked_graphical_set = data.raw["gui-style"].default.th_button_tag_group_selected.default_graphical_set

-- [[ Output ]]

add_style(
  "th_textbox_output_small",
  {
    type = "textbox_style",
    parent = "stretchable_textfield"
    -- width = magical_number,
  }
)

add_style(
  "th_textbox_output_big",
  {
    type = "textbox_style",
    parent = "stretchable_textfield",
    -- width = magical_number,
    height = 160,
    rich_text_setting = "disabled"
  }
)

add_style(
  "th_button_output_expand",
  {
    type = "button_style",
    parent = "frame_button",
    size = 24,
    top_margin = 2
  }
)
add_style(
  "th_button_output_expand_selected",
  {
    type = "button_style",
    parent = "frame_button",
    size = 24,
    top_margin = 2
  }
)

-- selected buttons = like regular buttons, but have the selected and default swapped
local button_sel = data.raw["gui-style"].default.th_button_output_expand_selected
button_sel.default_graphical_set = data.raw["gui-style"].default.frame_button.selected_graphical_set
